priorities:
- name: "Pre-receive secret detection"
  url: ""
  group: "Secure:Secret Detection"
  estimated_size: "L"
  comments: >-
    This paves the way for us to do other types of real-time scanning in the IDE. There are still a number of architectural decisions that we need to make that will impact implementation of other Static Analysis scanning types.
- name: "Write guidance on meeting SLSA (Supply-chain Levels for Software Artifacts) L3 requirements with GitLab"
  url: "https://docs.google.com/document/d/1UqmG5XkafBnK31Ow1Du8LbkuihwhNw3agDBxhaKtcOw"
  group: "Govern - SSCS Working Group"
  estimated_size: "S"
  comments: >-
    This item can be timeboxed to 3 days or less.  This item also has some time urgency as we have a wave of customers asking about [SLSA compliance](https://slsa.dev/spec/v1.0/) and we really need a good response.  Essentially we just need a Principal Engineer to review the document, review the [SLSA requirements](https://slsa.dev/spec/v1.0/requirements), and help us work on some CI examples showing how we can achieve SLSA L3 compliance within GitLab.
- name: "Real-time scanning in the IDE"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/10283"
  group: "Secure:Static analysis and there might be a part for Threat Insights in the display of findings"
  estimated_size: "L"
  comments: >-
    We've done some research into this idea however the solution that we land on for pre-receive secret detection will greatly inform it. To get get ramped up on the project, it is advised to:

    1. Review the [feedback summary](https://gitlab.com/gitlab-org/gitlab/-/issues/405439#note_1496194444) in the technical discovery issue.

    2. Review the [decisions made, open questions, and next steps](https://gitlab.com/gitlab-org/gitlab/-/issues/405439#note_1498906430) in the technical discovery issue.

    3. Read through/watch the technical discovery review sync ([agenda](https://docs.google.com/document/d/1nRsp76-TCG3oUTk4245Lw_kvVuj-nc3tSsRCzhE5jXE/edit#bookmark=id.u9grud9mg21d), [recording](https://drive.google.com/drive/folders/15YXcVUPh6cCRVa2mqNSzMMZpIpLFOTJz))

    4. Watch [LSP integration](https://docs.google.com/document/d/1UnE57kr9XebwGyNp2hGTgdaR1B1LRDwqznjfhIJwhJE/edit?usp=sharing) brown bag ([recording](https://drive.google.com/file/d/1OkoQ_wKCnOzJF3w7pl8OEXKk71iCfENr/view) - shows secret detection POC working in the IDE)
- name: "Threat Insights Database Optimization"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/10819"
  group: "Govern - Threat Insights but will indirectly benefit all of Secure"
  estimated_size: "L"
  comments: >-
    Database performance is a blocker for our ability to do [advanced filtering on the Vulnerability Report](https://gitlab.com/groups/gitlab-org/-/epics/3429).  This includes several filters that the Secure stage needs (location and identifier).  Also, we have a very large and growing number of customers requesting the ability to [track vulnerabilities for all branches](https://gitlab.com/groups/gitlab-org/-/epics/3430) (which will also require adding an ability to filter by branch).  To support both of these initiatives, we likely will need to add partitioning to the table and possibly also [combine the Vulnerabilities and Vulnerabilities::Finding tables](https://gitlab.com/groups/gitlab-org/-/epics/10819).  These are large and very complex projects that are being worked on by the Threat Insights team but that might benefit from the help of a Principal Engineer. A spike to [Evaluate decomposing Secure related tables to a separate Postgres DB](https://gitlab.com/gitlab-org/gitlab/-/issues/427973) would be a good starting point.
- name: "Risk-based vulnerability prioritization"
  url: "https://gitlab.com/gitlab-org/gitlab/-/issues/390410"
  group: "Spans across Secure and Govern"
  estimated_size: "XL"
  comments: >-
    Some initial [research has been](https://gitlab.com/gitlab-org/ux-research/-/issues/2195) done in this area, but overall we don't yet have a good overarching vision for how this will work.
    
    Ideally we would have the full list of items in the ["Future product roadmap needs" section](https://gitlab.com/gitlab-org/gitlab/-/issues/390410#future-product-roadmap-needs) of the description in this research spike.  If we have all that data available, we would be able to much more 
    
    To implement this, we will likely need to take the following steps:

    1. Add more data into our External Database (Composition Analysis)

    2. Ingest that data as additional fields into the GitLab PostGres database (Composition Analysis)

    3. Display those fields in the Vulnerability Report (Threat Insights)

    4. Add filters to the Vulnerability Report (Threat Insights)

    5. Add filters to Security Policies (Security Policies)

    6. Create a new area in the UI where a risk-based priority score can be calculated (Ownership unclear)

    7. Surface the new priority score in the Vulnerability Report and in the Policy Editor (TI + SP teams respectively)
    
    The complexity of those steps and the span across multiple groups has limited our ability to make progress up to this point.  Ideally a Principal Engineer would help unblock that work and facilitate progress toward this feature.
- name: "Vulnerability alerting and notifications"
  url: "https://gitlab.com/gitlab-org/gitlab/-/issues/370054"
  group: "Govern but will indirectly benefit all of Secure"
  estimated_size: "L"
  comments: >-
    Ownership for this item is unclear between Threat Insights and Security Policies.  Now that we have Continuous Vulnerability Scanning in place, we really need a good vulnerability alerting solution.  Potentially this could be configured as a new type of security policy with customizable actions on where and when to send notifications about new vulnerabilities.  A Principal Engineer could help by proposing a path forward and helping to identify some small/iterative quick wins to unblock customers.
- name: "Organization-level security management"
  url: "https://gitlab.com/groups/gitlab-org/-/epics/9265"
  group: "All of Govern"
  estimated_size: "M"
  comments: >-
    Nearly all self managed customers need the ability to manage security across all of their top-level groups.  We have been waiting on the [Organization Object](https://gitlab.com/groups/gitlab-org/-/epics/9265) to become available; however, delivery has been repeatedly delayed as the new Organization object is intertwined with the GitLab Cells work to isolate database tables for scalability purposes.
    
    In a internal call on November 15 (See the private [notes doc](https://docs.google.com/document/d/18DdhrWjEObOl-bpAHn-svWpbcq2P57_Vn3y56kHNFCY/edit) and [recording](https://youtu.be/XMIVJlv6REY)) we explored the option of contributing just enough to the Organization Object so it could show up in the UI and so we could add our security features there.  The summary takeaway from that call is that the Organization object is so intertwined with the isolation work that this is not an option.
    
    Since the top priority here is to make the Vulnerability Report and Dependency List available for self managed customers across all their top-level groups, we may want to add those pages to the [Explore section of the product](https://docs.gitlab.com/ee/user/project/working_with_projects.html#explore-topics) as an intermediate solution.  Especially considering that the [timelines for the Organization object](https://docs.gitlab.com/ee/architecture/blueprints/organization/index.html#iteration-2-organization-mvc-experiment-fy25q2) are long as we are already very strong customer demand for this feature.  In theory this will be a relatively smaller task with a very big impact as we should be able to reuse the frontend that we already have at the Group level.
